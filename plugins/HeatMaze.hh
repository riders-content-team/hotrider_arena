#ifndef GAZEBO_PLUGINS_HEAT_MAZE_HH_
#define GAZEBO_PLUGINS_HEAT_MAZE_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

#include "ros/ros.h"
#include "hotrider_arena/HeatStatus.h"
#include "hotrider_arena/SetRoute.h"
#include "hotrider_arena/SingleHeat.h"
#include "std_srvs/Trigger.h"

namespace gazebo
{

  class GAZEBO_VISIBLE HeatMazePlugin : public WorldPlugin
  {
    // Constructor
    public: HeatMazePlugin();
    // Deconstructor
    public: virtual ~HeatMazePlugin();
    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::WorldPtr _world, sdf::ElementPtr _sdf);
    // Animate Battery
    private: virtual void Spin();
    // World Reset Callback
    public: virtual void OnReset();

    private: virtual void LoadMaze(sdf::ElementPtr _sdf);

    private: virtual void SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z);

    private: virtual void SpawnNumber(int number, double pose_x, double pose_y);

    private: virtual void SetRoute(int x, int y);

    private: virtual bool SetRouteCallback(hotrider_arena::SetRoute::Request &req,
      hotrider_arena::SetRoute::Response &res);

    private: virtual bool GetHeat(hotrider_arena::HeatStatus::Request &req,
      hotrider_arena::HeatStatus::Response &res);

    private: virtual bool ClearHeat(std_srvs::Trigger::Request &req,
      std_srvs::Trigger::Response &res);

    private: virtual bool ProcessSingleHeat(hotrider_arena::SingleHeat::Request &req,
      hotrider_arena::SingleHeat::Response &res);

    // Connection to World Update events.
    private: event::ConnectionPtr worldConnection, resetConnection;

    private: std::unique_ptr<ros::NodeHandle> rosNode;

    private: ros::ServiceServer spawnService, clearService, setRouteService, singleSpawnService;

    private: std::vector<std::string> number_models;

    private: physics::WorldPtr world;

    private: int x_dim, y_dim, heat_offset, crate_num, number_num;

    private: double origin_x, origin_y;

    private: int* Maze;

    private: int* HeatMap;

    private: int* NumberMap;
  };
}
#endif
