#include "HeatMaze.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(HeatMazePlugin)


// Constructor
HeatMazePlugin::HeatMazePlugin() : WorldPlugin()
{
}


// Deconstructor
HeatMazePlugin::~HeatMazePlugin()
{
}


// Runs once on initialization
void HeatMazePlugin::Load(physics::WorldPtr _world, sdf::ElementPtr _sdf)
{
    this->world = _world;
    this->LoadMaze(_sdf);
    //Start Condition
    this->heat_offset = -1;
    this->crate_num = 0;
    this->number_num = 0;

    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, "spawn_model_service_node",
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to
    // the Gazebo node
    this->rosNode.reset(new ros::NodeHandle());

    this->spawnService = this->rosNode->advertiseService<hotrider_arena::HeatStatus::Request, hotrider_arena::HeatStatus::Response>
        ("get_heats", std::bind(&HeatMazePlugin::GetHeat, this, std::placeholders::_1, std::placeholders::_2));

    this->clearService = this->rosNode->advertiseService<std_srvs::Trigger::Request, std_srvs::Trigger::Response>
        ("clear_heats", std::bind(&HeatMazePlugin::ClearHeat, this, std::placeholders::_1, std::placeholders::_2));

    this->setRouteService = this->rosNode->advertiseService<hotrider_arena::SetRoute::Request, hotrider_arena::SetRoute::Response>
        ("set_destination", std::bind(&HeatMazePlugin::SetRouteCallback, this, std::placeholders::_1, std::placeholders::_2));

    /*
    this->singleSpawnService = this->rosNode->advertiseService<hotrider_arena::SingleHeat::Request, hotrider_arena::SingleHeat::Response>
        ("get_single_heat", std::bind(&HeatMazePlugin::ProcessSingleHeat, this, std::placeholders::_1, std::placeholders::_2));
    */
   
    // Connect to the world update event.
    this->worldConnection = event::Events::ConnectWorldUpdateBegin(
            std::bind(&HeatMazePlugin::Spin, this));

    // Connect to the world update event.
    this->resetConnection = event::Events::ConnectWorldReset(
            std::bind(&HeatMazePlugin::OnReset, this));

    /*
    //Left for debug purposes
    for (int i = 0; i < this->y_dim; i++)
    {
        for (int j = 0; j < this->x_dim; j++)
        {
            std::cout << this->HeatMap[this->x_dim * i + j] << " ";
        }
        std::cout << '\n';
    }
    */
}

void HeatMazePlugin::Spin()
{
  if (ros::ok())
  {
    ros::spinOnce();
  }
}

void HeatMazePlugin::OnReset()
{
    for (std::vector<std::string>::iterator ni = this->number_models.begin();
      ni != this->number_models.end(); ++ni)
    {
        this->world->RemoveModel(*ni);
    }
    this->number_models.clear();
    //Start Condition
    this->heat_offset = -1;
    this->number_num = 0;

    std::memcpy(this->HeatMap, this->Maze, this->x_dim * this->y_dim * sizeof(int));
    std::memcpy(this->NumberMap, this->Maze, this->x_dim * this->y_dim * sizeof(int));
}


bool HeatMazePlugin::ClearHeat(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
{
    this->OnReset();

    res.success = true;
    res.message = "";

    return true;
}


void HeatMazePlugin::LoadMaze( sdf::ElementPtr sdf)
{
    std::string dimensions;
    std::string total_rows;


    if (sdf->HasElement("maze_dimensition"))
    {
        dimensions = sdf->Get<std::string>("maze_dimensition");

        std::string delimiter = " ";
        std::string x_dim_string = dimensions.substr(0, dimensions.find(delimiter));
        std::string y_dim_string = dimensions.substr(dimensions.find(delimiter) + 1, dimensions.length() - 1);
        this->x_dim = std::stoi(x_dim_string);
        this->y_dim = std::stoi(y_dim_string);
        this->Maze = (int*)std::malloc(this->x_dim * this->y_dim * sizeof(int));
        this->HeatMap = (int*)std::malloc(this->x_dim * this->y_dim * sizeof(int));
        this->NumberMap = (int*)std::malloc(this->x_dim * this->y_dim * sizeof(int));
    }


    if (sdf->HasElement("origin"))
    {
        std::string origin_string = sdf->Get<std::string>("origin");
        std::string delimiter = " ";

        std::string origin_x_string = origin_string.substr(0, origin_string.find(delimiter));
        std::string origin_y_string = origin_string.substr(origin_string.find(delimiter) + 1, origin_string.length() - 1);

        this->origin_y = std::stod(origin_y_string);
        this->origin_x = std::stod(origin_x_string);
    }


    if (sdf->HasElement("rows_in_binary"))
    {
        total_rows = sdf->Get<std::string>("rows_in_binary");
        std::string binary_row;

        for (int i = 0; i < this->y_dim; i++)
        {
            binary_row = total_rows.substr((this->y_dim - i - 1) * (this->x_dim + 1), this->x_dim + 1);

            for (int j = 0; j < this->x_dim; j++)
            {
                if (binary_row[j] == '1')
                {
                  //  Remove crate spawn = part of model now
               //   this->SpawnModel("crate", "crate_" + std::to_string(this->crate_num), (double)j + origin_x, (double)i + origin_y, 0);
                  this->crate_num++;
                  this->Maze[this->x_dim * i + j] = -1000;
                }
            }
        }
        std::memcpy(this->HeatMap, this->Maze, this->x_dim * this->y_dim * sizeof(int));
        std::memcpy(this->NumberMap, this->Maze, this->x_dim * this->y_dim * sizeof(int));
    }
}


void HeatMazePlugin::SpawnModel(std::string model_name, std::string spawn_name, double pose_x, double pose_y, double pose_z)
{
    std::string pose_x_str = std::to_string(pose_x);
    std::string pose_y_str = std::to_string(pose_y);
    std::string pose_z_str = std::to_string(pose_z);

    std::string sdf_file_string = "<?xml version='1.0' ?> "
                                  "<sdf version='1.5'>"
                                  "<include>"
                                  "<pose>" + pose_x_str + " " + pose_y_str + pose_z_str + " 0 0 0" + "</pose>"
                                  "<name> " + spawn_name + "</name>"
                                  "<uri>model://" + model_name + "</uri>"
                                  "</include>"
                                  "</sdf>";

    this->world->InsertModelString(sdf_file_string);
}


void HeatMazePlugin::SpawnNumber(int number, double pose_x, double pose_y)
{
  double number_height = 0.1;
  double number_distance = 0.4;

  if(number >= 0)
  {
    if(number >= 10)
    {
        int primary_number = number % 10;
        int secondary_number = number / 10;

        std::string primary_model_name = "number_" + std::to_string(primary_number);
        std::string secondary_model_name = "number_" + std::to_string(secondary_number);

        std::string primary_spawn_name = primary_model_name + "_" + std::to_string(number_num);
        this->number_num++;
        std::string secondary_spawn_name = secondary_model_name + "_" + std::to_string(number_num);
        this->number_num++;

        this->number_models.push_back(primary_spawn_name);
        this->number_models.push_back(secondary_spawn_name);

        this->SpawnModel(primary_model_name, primary_spawn_name, pose_x + number_distance/2, pose_y, number_height);
        this->SpawnModel(secondary_model_name, secondary_spawn_name, pose_x - number_distance/2, pose_y, number_height);

    }
    else
    {
        std::string model_name = "number_" + std::to_string(number);
        std::string spawn_name = model_name + "_" + std::to_string(number_num);
        this->number_num++;
        this->number_models.push_back(spawn_name);
        this->SpawnModel(model_name, spawn_name, pose_x, pose_y, number_height);
    }
      
  }
  else
  {
    if(number <= -10)
    {
        int primary_number = -(number % 10);
        int secondary_number = -(number / 10);

        std::string dash_model_name = "number_dash";
        std::string primary_model_name = "number_" + std::to_string(primary_number);
        std::string secondary_model_name = "number_" + std::to_string(secondary_number);

        std::string dash_spawn_name = dash_model_name + "_" + std::to_string(number_num);
        this->number_num++;
        std::string primary_spawn_name = primary_model_name + "_" + std::to_string(number_num);
        this->number_num++;
        std::string secondary_spawn_name = secondary_model_name + "_" + std::to_string(number_num);
        this->number_num++;

        this->number_models.push_back(dash_spawn_name);
        this->number_models.push_back(primary_spawn_name);
        this->number_models.push_back(secondary_spawn_name);

        this->SpawnModel(primary_model_name, primary_spawn_name, pose_x + 0.3, pose_y, number_height);
        this->SpawnModel(secondary_model_name, secondary_spawn_name, pose_x - 0.1, pose_y, number_height);
        this->SpawnModel(dash_model_name, dash_spawn_name, pose_x - 0.3, pose_y, number_height);

    }
    else
    {
        std::cout << std::to_string(number)<<std::endl;
        std::cout << std::to_string(-number)<<std::endl;

        std::string dash_model_name = "number_dash";
        std::string model_name = "number_" + std::to_string(-number);
        
        std::string dash_spawn_name = dash_model_name + "_" + std::to_string(number_num);
        this->number_num++;
        std::string spawn_name = model_name + "_" + std::to_string(number_num);
        this->number_num++;

        this->number_models.push_back(dash_spawn_name);
        this->number_models.push_back(spawn_name);

        this->SpawnModel(dash_model_name, dash_spawn_name, pose_x - 0.2, pose_y, number_height);
        this->SpawnModel(model_name, spawn_name, pose_x + 0.2, pose_y, number_height);
    }

  }
  
}


bool HeatMazePlugin::SetRouteCallback(hotrider_arena::SetRoute::Request &req, hotrider_arena::SetRoute::Response &res)
{
    int destination_point_x = req.destination_x - this->origin_x;
    int destination_point_y = req.destination_y - this->origin_y;

    if(this->Maze[this->x_dim * (destination_point_y) + destination_point_x] == -1000)
    {
        res.status = false;
        return true;
    }

    this->SetRoute(destination_point_x, destination_point_y);

    res.status = true;
    return true;
}


void HeatMazePlugin::SetRoute(int x, int y)
{
    bool way_out = true;
    int current_heat = 1;
    std::array<int, 2> point = {x, y};
    std::vector<std::array<int, 2>> points;
    std::vector<std::array<int, 2>> temp_points;
    points.push_back(point);
    this->HeatMap[this->x_dim * (point[1]) + point[0]] = current_heat;

    while(way_out)
    {
        current_heat++;
        way_out = false;
        for(std::vector<std::array<int, 2>>::iterator pi = points.begin();
          pi != points.end(); ++pi)
        {
            //North
            point[0] = (*pi).data()[0];
            point[1] = (*pi).data()[1] + 1;
            if (!this->HeatMap[this->x_dim * (point[1]) + point[0]])
            {
                this->HeatMap[this->x_dim * (point[1]) + point[0]] = current_heat;
                temp_points.push_back(point);
                way_out = true;
            }
            //East
            point[0] = (*pi).data()[0] + 1;
            point[1] = (*pi).data()[1];
            if (!this->HeatMap[this->x_dim * (point[1]) + point[0]])
            {
                this->HeatMap[this->x_dim * (point[1]) + point[0]] = current_heat;
                temp_points.push_back(point);
                way_out = true;
            }
            //South
            point[0] = (*pi).data()[0];
            point[1] = (*pi).data()[1] - 1;
            if (!this->HeatMap[this->x_dim * (point[1]) + point[0]])
            {
                this->HeatMap[this->x_dim * (point[1]) + point[0]] = current_heat;
                temp_points.push_back(point);
                way_out = true;
            }
            //West
            point[0] = (*pi).data()[0] - 1;
            point[1] = (*pi).data()[1];
            if (!this->HeatMap[this->x_dim * (point[1]) + point[0]])
            {
                this->HeatMap[this->x_dim * (point[1]) + point[0]] = current_heat;
                temp_points.push_back(point);
                way_out = true;
            }
        }
        points.clear();
        points = temp_points;
        temp_points.clear();
    }
}


bool HeatMazePlugin::GetHeat(hotrider_arena::HeatStatus::Request &req, hotrider_arena::HeatStatus::Response &res)
{
    int point[2];
    int pos_x = (int)(req.x - this->origin_x);
    int pos_y = (int)(req.y - this->origin_y);

    point[0] = pos_x;
    point[1] = pos_y;

    //Check Current Heat
    if (this->heat_offset == -1)
    {
        this->heat_offset = this->HeatMap[this->x_dim * (point[1]) + point[0]] + 1;
    }

    //Center
    if (!this->NumberMap[this->x_dim * (point[1]) + point[0]])
    {
        int current_heat = this->heat_offset - this->HeatMap[this->x_dim * (point[1]) + point[0]];
        this->NumberMap[this->x_dim * (point[1]) + point[0]] = current_heat;
        if(current_heat > -1000)
        {
            this->SpawnNumber(current_heat, point[0] + this->origin_x, point[1] + this->origin_y);
        }
    }
    res.center_heat = this->NumberMap[this->x_dim * (point[1]) + point[0]];

    //North
    point[0] = pos_x;
    point[1] = pos_y + 1;
    if (!this->NumberMap[this->x_dim * (point[1]) + point[0]])
    {
        int current_heat = this->heat_offset - this->HeatMap[this->x_dim * (point[1]) + point[0]];
        this->NumberMap[this->x_dim * (point[1]) + point[0]] = current_heat;
        if(current_heat > -1000)
        {
            this->SpawnNumber(current_heat, point[0] + this->origin_x, point[1] + this->origin_y);
        }
    }
    res.north_heat = this->NumberMap[this->x_dim * (point[1]) + point[0]];

    //East
    point[0] = pos_x + 1;
    point[1] = pos_y;
    if (!this->NumberMap[this->x_dim * (point[1]) + point[0]])
    {
        int current_heat = this->heat_offset - this->HeatMap[this->x_dim * (point[1]) + point[0]];
        this->NumberMap[this->x_dim * (point[1]) + point[0]] = current_heat;
        if(current_heat > -1000)
        {
            this->SpawnNumber(current_heat, point[0] + this->origin_x, point[1] + this->origin_y);
        }
    }
    res.east_heat = this->NumberMap[this->x_dim * (point[1]) + point[0]];

    //South
    point[0] = pos_x;
    point[1] = pos_y - 1;
    if (!this->NumberMap[this->x_dim * (point[1]) + point[0]])
    {
        int current_heat = this->heat_offset - this->HeatMap[this->x_dim * (point[1]) + point[0]];
        this->NumberMap[this->x_dim * (point[1]) + point[0]] = current_heat;
        if(current_heat > -1000)
        {
            this->SpawnNumber(current_heat, point[0] + this->origin_x, point[1] + this->origin_y);
        }
    }
    res.south_heat = this->NumberMap[this->x_dim * (point[1]) + point[0]];

    //West
    point[0] = pos_x - 1;
    point[1] = pos_y;
    if (!this->NumberMap[this->x_dim * (point[1]) + point[0]])
    {
        int current_heat = this->heat_offset - this->HeatMap[this->x_dim * (point[1]) + point[0]];
        this->NumberMap[this->x_dim * (point[1]) + point[0]] = current_heat;
        if(current_heat > -1000)
        {
            this->SpawnNumber(current_heat, point[0] + this->origin_x, point[1] + this->origin_y);
        }
    }
    res.west_heat = this->NumberMap[this->x_dim * (point[1]) + point[0]];

    return true;
}

bool HeatMazePlugin::ProcessSingleHeat(hotrider_arena::SingleHeat::Request &req, hotrider_arena::SingleHeat::Response &res)
{
    std::cout << "single heat girdi"<<std::endl;

    double relative_pose_x = req.x;
    double relative_pose_y = req.y;
    std::string number_model = req.number_name;
    int number = std::stoi(number_model);

    double spawn_x = relative_pose_x + this->origin_x;
    double spawn_y = relative_pose_y + this->origin_y;

    this->SpawnNumber(number, spawn_x, spawn_y);

    this->HeatMap[this->x_dim * ((int)relative_pose_y) + (int)relative_pose_x] = number;

    res.center_heat = this->HeatMap[this->x_dim * ((int)relative_pose_y) + (int)relative_pose_x];

    return true;
}
